package main

import (
	"bytes"
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"net/http"
	"net/url"
	"os"
	"os/signal"
	"time"

	"github.com/faiface/beep"
	"github.com/gorilla/websocket"

	"github.com/faiface/beep/speaker"

	"github.com/faiface/beep/mp3"
)

type Frame struct {
	SampleRate  int          `json:"sample_rate"`
	NumChannels int          `json:"num_channels"`
	Precision   int          `json:"precision"`
	FrameData   [][2]float64 `json:"frame_data"`
}

type WebsocketStreamer struct {
	inChan    chan Frame
	currBuf   [][2]float64
	currIndex int
}

func (ws *WebsocketStreamer) Stream(samples [][2]float64) (n int, ok bool) {
	for i := range samples {
		if len(ws.currBuf) <= ws.currIndex {
			frame := <-ws.inChan
			log.Printf("Frame receive")
			ws.currBuf = frame.FrameData
			ws.currIndex = 0
		}

		samples[i] = ws.currBuf[ws.currIndex]
		ws.currIndex++
	}

	return len(samples), true
}

func (ws WebsocketStreamer) Err() error {
	return nil
}

func src(c *websocket.Conn, doneChan chan struct{}) {
	f, err := os.Open("music.mp3")
	if err != nil {
		log.Fatalf("Open: %v", err)
	}

	streamer, format, err := mp3.Decode(f)
	if err != nil {
		log.Fatalf("Decode: %v", err)
	}
	defer streamer.Close()

	frame := Frame{
		SampleRate:  int(format.SampleRate),
		NumChannels: format.NumChannels,
		Precision:   format.Precision,
		FrameData:   make([][2]float64, int(format.SampleRate)),
	}

	loop := beep.Loop(-1, streamer)

	nextFrame := time.Now()

	for {
		select {
		case <-doneChan:
			return
		default:
		}

		until := time.Until(nextFrame)
		time.Sleep(until)

		loop.Stream(frame.FrameData)
		w, err := c.NextWriter(websocket.BinaryMessage)
		if err != nil {
			log.Printf("Writer: %v", err)
			return
		}

		if err = json.NewEncoder(w).Encode(frame); err != nil {
			log.Printf("Encoder: %v", err)
			return
		}

		if err = w.Close(); err != nil {
			log.Printf("Close: %v", err)
		}

		nextFrame = nextFrame.Add(time.Second)

		log.Printf("Frame send")
	}

}

func sink(sinkChan chan Frame, doneChan chan struct{}) {
	log.Printf("Sink wait")

	var frame Frame

	select {
	case frame = <-sinkChan:
	case <-doneChan:
		return
	}

	log.Printf("Frame receive")

	sampleRate := beep.SampleRate(frame.SampleRate)
	log.Printf("Init samplerate: %v", sampleRate)
	err := speaker.Init(sampleRate, sampleRate.N(time.Second/10))
	if err != nil {
		log.Printf("Speaker init: %v", err)
		return
	}

	wss := &WebsocketStreamer{
		inChan:  sinkChan,
		currBuf: frame.FrameData,
	}

	log.Printf("Play")
	done := make(chan struct{})
	speaker.Play(beep.Seq(wss, beep.Callback(func() {
		close(done)
	})))

	<-done

	log.Printf("Done")
}

func main() {
	interrupt := make(chan os.Signal, 1)
	signal.Notify(interrupt, os.Interrupt)

	addr := flag.String("addr", "localhost:80", "Server address")
	channel := flag.String("channel", "", "Channel name")
	source := flag.Bool("source", false, "Become source")
	wss := flag.Bool("wss", false, "Use secure websocket")

	flag.Parse()

	u := url.URL{
		Scheme: "ws",
		Host:   *addr,
		Path:   fmt.Sprintf("/%v", *channel),
	}

	if *wss {
		u.Scheme = "wss"
	}

	log.Printf("Connecting to %v", u.String())

	dialer := websocket.Dialer{
		Proxy:             http.ProxyFromEnvironment,
		HandshakeTimeout:  45 * time.Second,
		EnableCompression: true,
	}

	c, _, err := dialer.Dial(u.String(), nil)
	if err != nil {
		log.Fatalf("Dial: %v", err)
	}
	defer c.Close()

	c.SetPingHandler(func(string) error {
		c.WriteMessage(websocket.PongMessage, nil)
		return nil
	})

	sinkChan := make(chan Frame)
	doneChan := make(chan struct{})

	if *source {
		go src(c, doneChan)
		go func() {
			for {
				select {
				case <-sinkChan:
				case <-doneChan:
					return
				}
			}
		}()
	} else {
		go sink(sinkChan, doneChan)
	}

	go func() {
		for {
			_, message, err := c.ReadMessage()
			if err != nil {
				if websocket.IsUnexpectedCloseError(err, websocket.CloseNormalClosure, websocket.CloseGoingAway, websocket.CloseAbnormalClosure) {
					log.Fatalf("Read: %v", err)
				}
				os.Exit(0)
			}

			frame := Frame{}
			err = json.NewDecoder(bytes.NewReader(message)).Decode(&frame)
			if err != nil {
				log.Printf("Decode: %v", err)
				continue
			}

			sinkChan <- frame
		}
	}()

	go func() {
		defer close(doneChan)

		<-interrupt

		err := c.WriteMessage(websocket.CloseMessage, websocket.FormatCloseMessage(websocket.CloseNormalClosure, ""))
		if err != nil {
			log.Printf("Write close: %v", err)
		}
	}()

	<-doneChan
}

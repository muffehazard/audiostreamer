module audiostreamer

go 1.12

require (
	github.com/faiface/beep v1.0.1
	github.com/gorilla/websocket v1.4.0
)
